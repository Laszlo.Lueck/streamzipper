﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Optional;
using Optional.Unsafe;
using Quartz;
using Quartz.Impl;

namespace StreamZipper.Services;

public static class Utils
{
    public static string Concat(this IEnumerable<string> source, string separator = "") => string.Concat(source);
        
    public static void ForEach<TKey, TValue>(this IEnumerable<(TKey, TValue)> source, Action<TKey, TValue> action)
    {
        foreach (var value in source)
        {
            action.Invoke(value.Item1, value.Item2);
        }
    }

    public static void ForEach<TIn>(this IEnumerable<TIn> source, Action<TIn> action)
    {
        foreach (var tIn in source)
        {
            action.Invoke(tIn);
        }
    }

    public static Task WrapAsync(this Action action) => Task.Run(action.Invoke);
        
    public static Task<TIn> WrapAsync<TIn>(this TIn inner) => Task.Run(() => inner);

    public static void MatchSome<TKey, TValue>(this Option<(TKey, TValue)> sourceOpt, Action<TKey, TValue> action)
    {
        if (!sourceOpt.HasValue) return;
        var (item1, value) = sourceOpt.ValueOrFailure();
        action.Invoke(item1, value);
    }
        
    public static async Task<Option<IScheduler>> StdSchedulerByName(string schedulerName)
    {
        var schedulerFactory = new StdSchedulerFactory();
        var schedulerOpt = await schedulerFactory.GetScheduler(schedulerName);
        return schedulerOpt.SomeNotNull();
    }
        
        
}
﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Quartz;
using Serilog;
using StreamZipper.Configuration;
using StreamZipper.Logging;

namespace StreamZipper.Services;

[DisallowConcurrentExecution]
public class ProcessJob : IJob
{
    private readonly Configuration.Configuration _configuration;
    private readonly ILogger _logger;

    public ProcessJob(IConfiguration configuration)
    {
        AppConfigurationBuilder.Build(configuration, out _configuration);
        _logger = LoggerBuilder.GetLogger<ProcessJob>();
    }

    public async Task Execute(IJobExecutionContext context)
    {
        Func<Task> toProcess = async () =>
        {
            _logger.Information("start download new file");
            var filePathOpt = await Processor.Process(_configuration.SchedulerConfig);
            var resultBool = await Processor.ProcessFileFromPath(filePathOpt, _configuration.SchedulerConfig);

            _logger.Information("extract files from snapshot finished with status {ResultBool}",
                resultBool);
            _logger.Information("finished job");
        };

        await toProcess
            .Invoke();
    }
}
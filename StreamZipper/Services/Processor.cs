﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ionic.Zip;
using Optional;
using Optional.Collections;
using Serilog;
using StreamZipper.Configuration;
using StreamZipper.Logging;

namespace StreamZipper.Services;

sealed record StorageResult(string FolderName, string FileName);

public static class Processor
{
    private static readonly ILogger Logger = LoggerBuilder.GetLogger(typeof(Processor));

    public static readonly Func<Option<string>, SchedulerConfiguration, Task<bool>> ProcessFileFromPath =
        async (filePathOpt, config) =>
        {
            return await filePathOpt.Match(async zipFile =>
            {
                Logger.Information("process zip file in path {Zipfile}", zipFile);
                var targetPath = config.TargetPath;
                Logger.Information("using {Target} to store the result", targetPath);
                Logger.Information("looking for content in target {Target} and remove them", targetPath);
                var fileList = Directory.GetFiles(targetPath);
                if (fileList.Length > 0)
                {
                    fileList.ForEach(file =>
                    {
                        Logger.Information("try to remove file {File}", file);
                        File.Delete(file);
                    });
                }

                Action extractZipFile = () =>
                {
                    using var zipFileO = ZipFile.Read(zipFile);
                    zipFileO.ExtractProgress += ProcessExtractState;
                    zipFileO.ExtractAll(targetPath);
                };

                await extractZipFile
                    .WrapAsync();

                return true;
            }, () => false.WrapAsync());
        };

    private static void ProcessExtractState(object sender, ExtractProgressEventArgs e)
    {
        switch (e.EventType)
        {
            case ZipProgressEventType.Extracting_BeforeExtractEntry:
                Logger.Information("extracting file: {Ex}", e.CurrentEntry.FileName);
                break;
        }
    }

    public static async Task<Option<string>> Process(SchedulerConfiguration configuration)
    {
        if (!Directory.Exists(configuration.StoreLocation))
        {
            Logger.Information("directory {Folder} does not exist, lets create them", configuration.StoreLocation);
            Directory.CreateDirectory(configuration.StoreLocation);
        }

        if (!Directory.Exists(configuration.TargetPath))
        {
            Logger.Information("directory {Folder} does not exist, lets create them", configuration.TargetPath);
            Directory.CreateDirectory(configuration.TargetPath);
        }

        var resp = await RemoteService.DownloadDataAsStream(configuration.Url, configuration.UserAgent, configuration.TimeOutSec);
        Logger.Information("St: {Status}", resp.Status);
        resp.HttpResponseHeader.ForEach((key, value) =>
        {
            Logger.Information("K: {Key} :: V: {Value}", key, value);
        });

        return await resp.Payload.Match(async payload =>
            {
                var toStore = resp.HttpResponseHeader.FirstOrNone(kv => kv.Item1 is "Content-Disposition").Match(
                    kv =>
                    {
                        var fileNameRegex = new Regex("filename=\"(.*?)\"");
                        var createDateRegex = new Regex("creation-date=\"(.*?)\"");
                        var fileNameMatch = fileNameRegex.Match(kv.Item2);
                        var createDateMatch = createDateRegex.Match(kv.Item2);
                        var fileName = fileNameMatch.Groups[1].ToString().Length == 0
                            ? "Standardexport_standard.zip"
                            : fileNameMatch.Groups[1].ToString();
                        var createDate = createDateMatch.Groups[1].ToString().Length == 0
                            ? DateTime.Now.ToString("R")
                            : createDateMatch.Groups[1].ToString();
                        return new StorageResult(createDate, fileName);
                    },
                    () => new StorageResult(DateTime.Now.ToString("R"), "Standardexport_standard.zip"));

                var storeDir = configuration.StoreLocation +
                               Convert.ToDateTime(toStore.FolderName).ToString("yyyyMMddHHmmss");
                if (!Directory.Exists(storeDir))
                {
                    Logger.Information("create store directory {StoreDir}", storeDir);
                    Directory.CreateDirectory(storeDir);
                }

                Logger.Information("store result to {StoreUrl}", storeDir);
                var filePath = $"{storeDir}/{toStore.FileName}";
                var f = File.OpenWrite(filePath);
                await payload.CopyToAsync(f);
                await f.FlushAsync();
                f.Close();
                await f.DisposeAsync();
                Logger.Information("successfully written {Filesize} bytes to store location", payload.Length);
                await payload.FlushAsync();
                payload.Close();
                await payload.DisposeAsync();
                return filePath.Some();
            },
            async () =>
            {
                Logger.Warning("could not get any payload to save, give up");
                return await Option.None<string>().WrapAsync();
            });
    }
}
﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using StreamZipper.Configuration;

namespace StreamZipper.Services;

public static class QuartzSchedulerExtensions
{
    public static void AddQuartzScheduler(this IServiceCollection services, IConfiguration configuration)
    {
        AppConfigurationBuilder.Build(configuration, out var cfg);
        services.AddQuartz(q =>
        {
            q.SchedulerName = cfg.SchedulerName;
            q.SchedulerId = cfg.SchedulerId;
            q.UseMicrosoftDependencyInjectionJobFactory();
        });


        services.AddQuartz(q =>
        {
            var jk = new JobKey(cfg.SchedulerConfig.JobName, cfg.SchedulerConfig.GroupName);
            q.AddJob<ProcessJob>(jk, p => p.WithDescription($"creating job for product data import"));
            q.AddTrigger(t =>
            {
                t.ForJob(jk)
                    .WithIdentity(cfg.SchedulerConfig.TriggerName, cfg.SchedulerConfig.GroupName)
                    .WithCronSchedule(cfg.SchedulerConfig.CronSchedule)
                    .WithDescription($"creating trigger for data import");
            });
        });

        services.AddQuartz(q =>
        {
            var jk = new JobKey(cfg.CleanupConfig.JobName, cfg.CleanupConfig.GroupName);
            q.AddJob<CleanupJob>(jk,
                p => p.WithDescription($"create job for cleanup old archives"));

            q.AddTrigger(t =>
            {
                t.ForJob(jk)
                    .WithIdentity(cfg.CleanupConfig.TriggerName, cfg.CleanupConfig.GroupName)
                    .WithCronSchedule(cfg.CleanupConfig.CronSchedule)
                    .WithDescription($"create trigger for cleanup old archives");
            });
        });

        services.AddQuartzServer(options => options.WaitForJobsToComplete = true);
    }
}
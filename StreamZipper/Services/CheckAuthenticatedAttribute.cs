﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Optional;
using StreamZipper.Configuration;

namespace StreamZipper.Services;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class CheckAuthenticatedAttribute : ActionFilterAttribute
{
    public override void OnActionExecuting(ActionExecutingContext context)
    {
        var config = context.HttpContext.RequestServices.GetService<IConfiguration>();
        AppConfigurationBuilder.Build(config, out var configuration);

        if (configuration.UseAuthentication)
        {
            context
                .HttpContext
                .Request
                .Headers["Authorization"]
                .SomeNotNull()
                .Match(authValue =>
                {
                    if (authValue == configuration.AuthToken) return;
                    context.Result = new UnauthorizedResult();
                }, () => context.Result = new UnauthorizedResult());
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Optional;
using Serilog;
using StreamZipper.Logging;

namespace StreamZipper.Services;

public static class RemoteService
{
    private static readonly ILogger Logger = LoggerBuilder.GetLogger(typeof(RemoteService));

    public class StatusFromHttpRequest
    {
        public int Status { get; set; }
        public IEnumerable<(string, string)> HttpResponseHeader { get; set; }

        public Option<MemoryStream> Payload { get; set; }
    }


    private static async Task<StatusFromHttpRequest> GetStatusResponse(HttpResponseMessage response)
    {
        static Option<Task<MemoryStream>> GetMemoryStream(Stream responseStream)
        {
            return responseStream.SomeNotNull().Map(async stream =>
            {
                var memStream = new MemoryStream();
                await stream.CopyToAsync(memStream);
                memStream.Position = 0;
                return memStream;
            });
        }

        var returnObject = new StatusFromHttpRequest
        {
            Status = (int) response.StatusCode,
            HttpResponseHeader =
                response.Headers.Select(header =>
                    (header.Key, header.Value.Concat(",")))
        };

        var ret = GetMemoryStream(await response.Content.ReadAsStreamAsync()).Match(async result =>
            {
                var str = await result;
                returnObject.Payload = str.Some();
                return returnObject;
            },
            () => returnObject.WrapAsync());


        return await ret;
    }

    public static async Task<StatusFromHttpRequest> DownloadDataAsStream(string url, string userAgent, int timeOut)
    {
        var sw = Stopwatch.StartNew();
        try
        {
            Logger.Information("download file from {Url}", url);
            Logger.Information("download with UserAgent {UserAgent}", userAgent);
            Logger.Information("set download timeout to {Timeout} s", timeOut);
            var fileReq = new HttpClient();
            fileReq.DefaultRequestHeaders.Add("UserAgent", userAgent);
            fileReq.Timeout = TimeSpan.FromSeconds(timeOut);
            var response = await fileReq.GetAsync(url);
            return await GetStatusResponse(response);
        }
        catch (WebException webException)
        {
            Logger.Error(webException, "An webException occured");
            return webException.Response.SomeNotNull().Match(response =>
                {
                    response.Headers.AllKeys.ForEach(header =>
                    {
                        Logger.Warning("Key: {HeaderKey}; Value: {HeaderValue}", header,
                            response.Headers.Get(header) ?? "");
                    });
                    return new StatusFromHttpRequest
                    {
                        Status = (int) webException.Status, HttpResponseHeader = Array.Empty<(string, string)>(),
                        Payload = Option.None<MemoryStream>()
                    };
                },
                () => new StatusFromHttpRequest
                {
                    Status = (int) webException.Status, HttpResponseHeader = Array.Empty<(string, string)>(),
                    Payload = Option.None<MemoryStream>()
                });
        }
        catch (Exception exception)
        {
            Logger.Error(exception, "an error occured while download data");
            return new StatusFromHttpRequest {Status = 500, HttpResponseHeader = Array.Empty<(string, string)>()};
        }
        finally
        {
            sw.Stop();
            Logger.Information("download resource {Url} as stream in {Elapsed:000} ms", url,
                sw.ElapsedMilliseconds);
        }
    }
}
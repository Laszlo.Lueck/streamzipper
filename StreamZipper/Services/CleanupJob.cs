﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Quartz;
using Serilog;
using StreamZipper.Configuration;
using StreamZipper.Logging;

namespace StreamZipper.Services;

[DisallowConcurrentExecution]
public class CleanupJob : IJob
{
    private readonly Configuration.Configuration _configuration;
    private readonly ILogger _logger;

    public CleanupJob(IConfiguration configuration)
    {
        AppConfigurationBuilder.Build(configuration, out _configuration);
        _logger = LoggerBuilder.GetLogger<CleanupJob>();
    }

    public async Task Execute(IJobExecutionContext context)
    {
        Func<Task> toProcess = async () =>
        {
            await Task.Run(() =>
            {
                _logger.Information("start processing cleanup backups oder than {Day} days",
                    _configuration.CleanupConfig.RemoveOlderThanDays);

                if (!Directory.Exists(_configuration.CleanupConfig.TargetPath))
                {
                    _logger.Warning("target directory {Directory} does not exist, exit processing", _configuration.CleanupConfig.TargetPath);
                }
                else
                {
                    var dateBeforeDelete = DateTime
                        .Now
                        .Date
                        .Subtract(TimeSpan.FromDays(_configuration.CleanupConfig.RemoveOlderThanDays));

                    _logger.Information("find directories starting with {DateStartBefore} and remove them", dateBeforeDelete);

                    Directory
                        .GetDirectories(_configuration.CleanupConfig.TargetPath)
                        .Where(directory => Directory.GetCreationTime(directory) <= dateBeforeDelete)
                        .ForEach(directory =>
                        {
                            _logger.Information("remove directory {Directory}", directory);
                            Directory.Delete(directory, true);
                        });

                }
                _logger.Information("finished processing cleanup old backups");
            });
        };


        await toProcess
            .Invoke();
    }
}
﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace StreamZipper.Services;

public class StartupParameter
{
    public DateTime StartTime { get; set; }
}
    
public static class StartupExtension
{
    public static void AddStartupParameter(this IServiceCollection serviceCollection, StartupParameter startupParameter)
    {
        serviceCollection.AddSingleton<StartupParameter>(startupParameter);
    }
        
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Optional;
using Optional.Collections;
using Optional.Linq;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;
using Serilog;
using StreamZipper.Configuration;
using StreamZipper.Logging;
using StreamZipper.Services;
using StreamZipper.To;
using FileInfo = StreamZipper.To.FileInfo;

namespace StreamZipper.Controllers;

[ApiController]
[Route("info/scheduler")]
[CheckAuthenticated]
public class SchedulerController : ControllerBase
{
    private readonly Configuration.Configuration _configuration;
    private readonly StartupParameter _startupParameter;
    private readonly ILogger _logger;
    private readonly SHA256 _sha256;

    public SchedulerController(IConfiguration configuration, StartupParameter startupParameter)
    {
        AppConfigurationBuilder.Build(configuration, out _configuration);
        _startupParameter = startupParameter;
        _logger = LoggerBuilder.GetLogger<SchedulerController>();
        _sha256 = SHA256.Create();
    }

    [Route("status")]
    [HttpGet]
    public async Task<StartupParameter> GetStatus()
    {
        return await _startupParameter.WrapAsync();
    }

    [Route("latest")]
    [HttpGet]
    public async Task<IEnumerable<FileInfo>> GetSnapshotInfo()
    {
        Func<IEnumerable<FileInfo>> doAction = () =>
        {
            try
            {
                return GetFileInfosFromDirectory(_configuration.SchedulerConfig.TargetPath);
            }
            catch (Exception exception)
            {
                _logger.Error(exception, "an error while getting all snapshot file info occured");
                return Array.Empty<FileInfo>();
            }
        };

        return await doAction.Invoke().WrapAsync();
    }

    private IEnumerable<FileInfo> GetFileInfosFromDirectory(string directory)
    {
        _logger.Information("try to get file information from {Target}", directory);
        var di = new DirectoryInfo(directory);
        const string dlPath = "/download/latest/";
        if (!di.Exists) return Array.Empty<FileInfo>();
        _logger.Information("getting file information from directory {Target}", di.FullName);
        return di.GetFiles().Select(file => new FileInfo
        {
            FileName = file.Name,
            CreateDate = file.CreationTime,
            FileSize = file.Length,
            Hash = _calculateHash(file, _sha256),
            Link = $"{dlPath}{file.Name}"
        });
    }

    private readonly Func<System.IO.FileInfo, SHA256, string> _calculateHash = (fileInfo, encryption) =>
    {
        var stringToHash = $"{fileInfo.Name}{fileInfo.Length}{fileInfo.CreationTime}";
        return encryption
            .ComputeHash(Encoding.UTF8.GetBytes(stringToHash))
            .Select(bt => bt.ToString("x2"))
            .Concat();
    };

    [Route("instantStart/{jobType}")]
    [HttpGet]
    public async Task<IActionResult> InstantStartJobByName(Enumerators.JobType jobType)
    {
        Func<Task<IActionResult>> doAction = async () =>
            jobType switch
            {
                Enumerators.JobType.Download => await StartJobByJobName(_configuration.SchedulerName,
                    _configuration.SchedulerConfig),
                Enumerators.JobType.Cleanup => await StartJobByJobName(_configuration.SchedulerName,
                    _configuration.CleanupConfig),
                _ => throw new ArgumentException($"unknown job type {jobType.ToString()} received, gave up")
            };

        return await doAction.Invoke();
    }

    private async Task<IActionResult> StartJobByJobName(string schedulerName, JobConfiguration configuration)
    {
        var schedulerOpt = await Utils.StdSchedulerByName(schedulerName);
        return await schedulerOpt.Match<Task<IActionResult>>(async scheduler =>
            {
                var triggerState = await (await TaskHelper.GetTriggerKeys(scheduler, configuration.GroupName))
                    .FirstOrNone()
                    .Match<Task<Either<bool, string>>>(
                        async triggerKey =>
                        {
                            var state = await scheduler.GetTriggerState(triggerKey);
                            _logger.Information("current state for trigger key {TriggerKey} is {State}",
                                triggerKey.Name, state);
                            return state switch
                            {
                                TriggerState.Blocked or TriggerState.Error =>
                                    "trigger state is blocked or error, gave up",
                                _ => true
                            };
                        },
                        async () => await true.WrapAsync());

                return await triggerState.Match<Task<IActionResult>>(async _ =>
                    {
                        var jobKey = new JobKey(configuration.JobName, configuration.GroupName);
                        await scheduler.TriggerJob(jobKey);
                        return Ok(new StartJobActionResult
                        {
                            ResultText = $"job {configuration.JobName} started",
                            Status = Enumerators.StartJobActionResultStatus.Success
                        });
                    },
                    async right => await Ok(new StartJobActionResult
                            { ResultText = right, Status = Enumerators.StartJobActionResultStatus.Failed })
                        .WrapAsync());
            },
            async () => await NotFound($"scheduler with name {schedulerName} not found").WrapAsync());
    }


    [Route("getStatistics")]
    [HttpGet]
    public async Task<Dictionary<string, SchedulerInfo>> GetSchedulerStatistics()
    {
        var schedulerFactory = new StdSchedulerFactory();
        return await (await schedulerFactory.GetScheduler(_configuration.SchedulerName))
            .SomeNotNull()
            .Match(async scheduler =>
                {
                    var schedulerConfig = _configuration.SchedulerConfig;
                    var schedulerReturn = new SchedulerInfo
                    {
                        CronString = schedulerConfig.CronSchedule
                    };

                    var triggerKeyOptScheduler =
                        (await TaskHelper.GetTriggerKeys(scheduler, schedulerConfig.GroupName))
                        .FirstOrNone();

                    TaskHelper.FillNextFireTimes(triggerKeyOptScheduler, schedulerReturn, scheduler);


                    var cleanupConfig = _configuration.CleanupConfig;
                    var cleanupReturn = new SchedulerInfo
                    {
                        CronString = cleanupConfig.CronSchedule
                    };

                    var triggerKeyOptCleanup = (await TaskHelper.GetTriggerKeys(scheduler, cleanupConfig.GroupName))
                        .FirstOrNone();
                    TaskHelper.FillNextFireTimes(triggerKeyOptCleanup, cleanupReturn, scheduler);


                    return await new Dictionary<string, SchedulerInfo>
                    {
                        { "Scheduler", schedulerReturn },
                        { "Cleanup", cleanupReturn }
                    }.WrapAsync();
                },
                async () => await new Dictionary<string, SchedulerInfo>().WrapAsync());
    }
}

internal static class TaskHelper
{
    public static readonly Action<Option<TriggerKey>, SchedulerInfo, IScheduler> FillNextFireTimes =
        (option, schedulerInfo, scheduler1) =>
        {
            option.Select(async triggerKey =>
            {
                schedulerInfo.TriggerState = (await scheduler1.GetTriggerState(triggerKey)).ToString();
                (await scheduler1.GetTrigger(triggerKey))
                    .SomeNotNull()
                    .GetTimes()
                    .MatchSome(
                        (nextFireTimeOpt, lastFireTimeOpt) =>
                        {
                            nextFireTimeOpt.MatchSome(nextFireTime => schedulerInfo.NextFireTime = nextFireTime);
                            lastFireTimeOpt.MatchSome(lastFireTime => schedulerInfo.LastFireTime = lastFireTime);
                        });
            });
        };


    private static Option<(Option<DateTime?>, Option<DateTime?>)> GetTimes(this Option<ITrigger> triggerOpt) =>
        triggerOpt.Map(GetTimesFunc);


    private static readonly Func<ITrigger, (Option<DateTime?>, Option<DateTime?>)> GetTimesFunc = trigger =>
    {
        var nextFireTimeOpt = trigger.GetNextFireTimeUtc().SomeNotNull()
            .Select(nextFireTime => nextFireTime?.UtcDateTime.ToLocalTime());
        var lastFireTimeOpt = trigger.GetPreviousFireTimeUtc().SomeNotNull()
            .Select(lastFireTime => lastFireTime?.UtcDateTime.ToLocalTime());

        return (nextFireTimeOpt, lastFireTimeOpt);
    };

    public static readonly Func<IScheduler, string, Task<IReadOnlyCollection<TriggerKey>>> GetTriggerKeys =
        async (scheduler, groupName) =>
            await scheduler.GetTriggerKeys(GroupMatcher<TriggerKey>.GroupEquals(groupName));
}
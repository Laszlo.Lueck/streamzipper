﻿using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MimeTypes;
using Serilog;
using StreamZipper.Configuration;
using StreamZipper.Logging;
using StreamZipper.Services;

namespace StreamZipper.Controllers;

[ApiController]
[Route("download/latest")]
[CheckAuthenticated]
public class DownloadController : ControllerBase
{
    private readonly ILogger _logger;
    private readonly Configuration.Configuration _configuration;

    public DownloadController(IConfiguration configuration)
    {
        _logger = LoggerBuilder.GetLogger<DownloadController>();
        AppConfigurationBuilder.Build(configuration, out _configuration);
    }

    [Route("{fileName}")]
    [HttpGet]
    public async Task DownloadFile(string fileName)
    {
        await Task.Run(async () =>
        {
            var filePathToDownload = $"{_configuration.SchedulerConfig.TargetPath}{fileName}";

            _logger.Information("try to download file {File}", filePathToDownload);

            if (!System.IO.File.Exists(filePathToDownload))
            {
                _logger.Information("file {File} does not exist, return 404", filePathToDownload);
                Response.StatusCode = 404;
                var returnString = $"can not find file in path {filePathToDownload}";
                await Response.Body.WriteAsync(Encoding.UTF8.GetBytes(returnString));
                return;
            }


            var returnFileName = HttpUtility.UrlEncode(fileName);
            var mt = MimeTypeMap.GetMimeType(fileName);
            Response.Headers.Add("Content-Disposition", $"inline; filename=\"{returnFileName}\"");
            Response.ContentType = mt;

            await using var fs = new FileStream(filePathToDownload, FileMode.Open, FileAccess.Read, FileShare.None,
                4096);
            await fs.CopyToAsync(Response.Body);
        });
    }
}
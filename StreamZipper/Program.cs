using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Enrichers.ShortTypeName;
using Serilog.Sinks.SystemConsole.Themes;

namespace StreamZipper;

public static class Program
{
    public static async Task Main(string[] args)
    {
        await CreateHostBuilder(args)
            .Build()
            .RunAsync();
    }

    private static IHostBuilder CreateHostBuilder(string[] args) =>
        Host
            .CreateDefaultBuilder(args)
            .ConfigureAppConfiguration((_, config) => { config.AddJsonFile("configuration.json", false, true); })
            .UseSerilog((_, loggerConfiguration) =>
            {
                loggerConfiguration
                    .Enrich.WithShortTypeName()
                    .MinimumLevel.Information()
                    .WriteTo.Console(theme: AnsiConsoleTheme.Code,
                        outputTemplate:
                        "[{Timestamp:yyy-MM-dd HH:mm:ss} {Level:u3} {ShortTypeName}] {Message:lj}{NewLine}{Exception}");
            })
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder
                    .ConfigureKestrel(options => options.Limits.KeepAliveTimeout = TimeSpan.FromHours(1))
                    .UseStartup<Startup>();
            });
}
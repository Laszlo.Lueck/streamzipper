﻿using System;
using System.Text.Json.Serialization;

namespace StreamZipper.To;

public class SchedulerInfo
{
    [JsonPropertyName("nextFireTime")] public DateTime? NextFireTime { get; set; }
    [JsonPropertyName("lastFireTime")] public DateTime? LastFireTime { get; set; }
    [JsonPropertyName("cronString")] public string CronString { get; set; }
        
    [JsonPropertyName("triggerState")] public string TriggerState { get; set; }

}
﻿using System.IO;

namespace StreamZipper.To;

public record DownloadFileResponse(Stream DownloadFileStream, string ReturnFileName, string ContentType);
﻿using System;
using System.Text.Json.Serialization;

namespace StreamZipper.To;

public class FileInfo
{
    [JsonPropertyName("fileName")] public string FileName { get; set; }
    [JsonPropertyName("createDate")] public DateTime CreateDate { get; set; }
        
    [JsonPropertyName("fileSize")] public long FileSize { get; set; }
        
    [JsonPropertyName("hash")] public string Hash { get; set; }
        
    [JsonPropertyName("link")] public string Link { get; set; }
}
﻿namespace StreamZipper.To;

public class Enumerators
{
    public enum StartJobActionResultStatus
    {
        Failed,
        Success
    }
        
    public enum JobType
    {
        Download,
        Cleanup
    }
        
}
﻿using System.Text.Json.Serialization;

namespace StreamZipper.To;

public class StartJobActionResult
{
    [JsonPropertyName("resultText")] public string ResultText { get; set; }
    [JsonPropertyName("resultStatus")] public Enumerators.StartJobActionResultStatus Status { get; set; }
}
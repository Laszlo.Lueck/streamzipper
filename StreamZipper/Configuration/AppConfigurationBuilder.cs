﻿using Microsoft.Extensions.Configuration;

namespace StreamZipper.Configuration;

public static class AppConfigurationBuilder
{
    public static void Build(IConfiguration configuration, out Configuration outConfiguration)
    {
        outConfiguration = new Configuration();
        configuration.GetSection("configuration").Bind(outConfiguration);
    }
}
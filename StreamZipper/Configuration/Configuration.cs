﻿using System.Text.Json.Serialization;

namespace StreamZipper.Configuration;

public class Configuration
{
    [JsonPropertyName("schedulerName")] public string SchedulerName { get; set; }
    [JsonPropertyName("schedulerId")] public string SchedulerId { get; set; }
        
    [JsonPropertyName("authToken")] public string AuthToken { get; set; }
        
    [JsonPropertyName("useAuthentication")] public bool UseAuthentication { get; set; }
        
    [JsonPropertyName("schedulerConfig")] public SchedulerConfiguration SchedulerConfig { get; set; }
        
    [JsonPropertyName("cleanupConfig")] public CleanupConfiguration CleanupConfig { get; set; }
}

public class JobConfiguration
{
    [JsonPropertyName("cronSchedule")] public string CronSchedule { get; set; }
    [JsonPropertyName("jobName")] public string JobName { get; set; }
    [JsonPropertyName("groupName")] public string GroupName { get; set; }
    [JsonPropertyName("triggerName")] public string TriggerName { get; set; }
}

public class CleanupConfiguration : JobConfiguration
{
    [JsonPropertyName("removeOlderThanDays")] public int RemoveOlderThanDays { get; set; }

    [JsonPropertyName("targetPath")] public string TargetPath { get; set; }
}

public class SchedulerConfiguration : JobConfiguration
{
    [JsonPropertyName("userAgent")] public string UserAgent { get; set; }
        
    [JsonPropertyName("url")] public string Url { get; set; }
    [JsonPropertyName("storeLocation")] public string StoreLocation { get; set; }

    [JsonPropertyName("targetPath")] public string TargetPath { get; set; }
    [JsonPropertyName("timeoutSec")] public int TimeOutSec { get; set; }
}
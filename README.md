# StreamZipper

Dieses Tool lädt eine Zip-Datei von einer URL herunter und entpackt diese Zip-Datei in ein
einen spezifischen Ordner.
Hiefür steht eine Konfigurations-Datei zur Verfügung.
Die Anwendung läuft in einem Docker-Container. Die einzige Voraussetzung 
für den Betrieb der Anwendung auf dem Host-System ist ein funktionierendes Docker-System
oder vergleichbares (Podman, Kubernetes) und eine Internet-Anbindung.

## Konfiguration
Die Konfiguration verwendet intern einen Mechanismus, bei dem Änderungen an der Konfiguration zur
Laufzeit direkt in der Anwendung aktualisiert werden. Dadurch wird kein Neustart des Containers benötigt.

````json
{
  "configuration": {
    "schedulerName": "StreamZipperScheduler",
    "schedulerId": "SZS_001",
    "authToken":"",
    "useAuthentication": false,
    "cleanupConfig": {
      "removeOlderThanDays": 10,
      "cronSchedule": "0 45 * ? * * *",
      "jobName": "SnapshotCleanupJob",
      "groupName": "SnapshotCleanupGroup",
      "triggerName": "SnapshotCleanupTrigger",
      "targetPath" : "./backup/snapshots/"
    },
    "schedulerConfig":{
      "userAgent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36",
      "cronSchedule": "0 15 1 ? * MON-FRI",
      "jobName": "SnapshotJob",
      "groupName": "SnapshotGroup",
      "triggerName": "SnapshotTrigger",
      "url": "https://api.itscope.com/2.1/t/API_KEY",
      "storeLocation" : "/app/backup/snapshots/",
      "targetPath" : "/app/data/snapshots/",
      "timeoutSec": 3600
    }
  }
}

````

### Grundsätzliches
Beim zeitlichen Herunterladen und entpacken der Dateien, habe ich mich für die Verwendung von (klassischen) Crons entschieden.
Das ist ein allgemein bekanntes Pattern und im Internet gibt es haufenweise Hilfe dazu.
Aktuell ist das Ding eingestellt auf:

Dateidownload: Täglich, Mo. - Fr. 1:15:00

Cleanup: Täglich jede Stunde zur Minute 45

#### Allgemeine Konfiguration:
- authToken: Wenn die Downloadfunktion genutzt wird, macht es Sinn, die Endpunkte abzusichern. Der Wert hier muss als Header-Value mit dem Key "Authorization" übergeben werden.
- useAuthentication: Wenn true, ist die Endpunktabsicherung aktiv. Wenn keiner oder ein ungültiger Authorization-Header mitgeliefert wird, wird als Ergebnis ein Unauthorized (401) zurückgesendet. 

#### Prozess-Konfiguration
Hier sind folgende Parameter wichtig:
- userAgent: Legt fest, wie sich die Anwendung auf der Gegenseite meldet. Default als Chrome-Browser
- url: Hier muss die jeweilige URL eingetragen werden in der sich die zip-Datei befindet
- timeoutSec: Zeit in Sekunden bis zum HTTP-Timeout. Da das herunterladen bei ITScope länger dauern kann, ist die Zeit hier mal auf 1h gesetzt.

#### Cleanup-Konfiguration
Hier sind folgende Parameter wichtig:
- removeOlderThanDays: Bei default 10 werden alle Archive älter 10 Tage entfernt. Entscheidend ist hier das Create-Date des Verzeichnisses.

### Kategorien
Die Scheduler-Konfiguration hat 2 Abschnitte.
1. schedulerConfig
2. cleanupConfig

In der Kategorie SchedulerConfig werden die Cron-Einstellungen für Feed und Snapshot-Verarbeitung vorgenommen.
Die Kategorie CleanupConfig beinhaltet Einstellungen zum zeitgesteuerten entfernen alter Sicherungen.
Der Wert ``removeOlderThanDays`` gibt an, das alle Sicherungen älter x-Tagen aus dem Backup-Verzeichnis entfernt werden,
um den Datenspeicher nicht voll zu müllen.

### Service-URLs
#### Instant Start
Sollte innertäglich ein Download bzw. Cleanup notwendig sein, stehen hierfür entsprechende URLs zur Verfügung.
1. ``/info/scheduler/instantStart/cleanup`` startet den Cleanup-Prozess, sofern nicht schon einer aktiv ist. Dieser Prozess kann immer nur einmal gleichzeitig laufen.
2. ``/info/scheduler/instantStart/download`` startet den Download- und Entpack-Protess, sofern nicht schon einer aktiv ist. Dieser Prozess kann immer nur einmal gleichzeitig laufen.

## Start und Verwendung als herunterladbare Ressource
Diese Option ist dann sinnvoll, wenn sich die Anwendung bzw. der Container außerhalb des Hosts befindet. Somit können von Host
die Dateien heruntergeladen und verarbeitet werden. Folgende Prozessschritte sind dafür notwendig.

P.S. Es wird nicht näher auf die Authentifizierung eingegangen. Der Download-Prozess ist immer der Gleiche.

1. Prüfung auf Dateien mittels Ressource ``/info/scheduler/latest``
    
    Mit der Ressource ``/info/scheduler/latest`` wird geprüft, ob sich herunterladbare Dateien im Anwendungskontext befinden.

    Folgendes Beispiel verdeutlicht den Aufbau des Ergebnisses:
    
```` json
[
{
"fileName": "historicalDataItem.csv",
"createDate": "2021-11-18T01:36:22.3729291+00:00",
"fileSize": 135,
"hash": "f98adf395d08ddaa074dde9f00470dbc223abbf9ac504fea5926e7ef42f4bba2",
"link": "/download/latest/historicalDataItem.csv"
},
{
"fileName": "project.csv",
"createDate": "2021-11-18T01:36:26.1849257+00:00",
"fileSize": 13467918,
"hash": "078be84338991184112f7e66b11c0c5d9e48b8b88f62200fbb43cdcb23c4f2b2",
"link": "/download/latest/project.csv"
},
{
"fileName": "attribute.csv",
"createDate": "2021-11-18T01:36:27.0569249+00:00",
"fileSize": 126,
"hash": "d699a1bcf207d6d71e3dbe23e29a0f2f6e4e66179b9023d9a572f46e4b68ee90",
"link": "/download/latest/attribute.csv"
},
{
"fileName": "attributeCluster.csv",
"createDate": "2021-11-18T01:36:26.0809258+00:00",
"fileSize": 795280,
"hash": "32282d1d50afb807471a78667777e64973fc18686656e9a645a5cbda3bd8e96e",
"link": "/download/latest/attributeCluster.csv"
},
{
"fileName": "products2Cluster.csv",
"createDate": "2021-11-18T01:36:27.0569249+00:00",
"fileSize": 74768521,
"hash": "f91ed63862699a2f64ec84251a00d4ac025883d381621827138ad49aa9129298",
"link": "/download/latest/products2Cluster.csv"
},
{
"fileName": "log.csv",
"createDate": "2021-11-18T01:36:22.1129293+00:00",
"fileSize": 35379064,
"hash": "a33cc00e3ca5c961272b82d806942458a9e1fb07adc20f3416a95018692a7ee5",
"link": "/download/latest/log.csv"
},
{
"fileName": "accessory.csv",
"createDate": "2021-11-18T01:36:22.3729291+00:00",
"fileSize": 32334177,
"hash": "dba35a6edd95928679593bfc7472175e8aef23b852d600988684a6a603b4fcf1",
"link": "/download/latest/accessory.csv"
},
{
"fileName": "product.csv",
"createDate": "2021-11-18T01:36:21.8609295+00:00",
"fileSize": 2522774294,
"hash": "10cfdc6fa7c451fc782f7337a6b5a7d46708caab9807d69d1619bc9db02b663d",
"link": "/download/latest/product.csv"
},
{
"fileName": "supplierItem.csv",
"createDate": "2021-11-18T01:36:26.0689258+00:00",
"fileSize": 349937616,
"hash": "7d3f1152d4122706b36f6d27ce60c289ce82b3112ab02ce59e3a54a1dd4ffe29",
"link": "/download/latest/supplierItem.csv"
}
]

````

Der Aufbau ist wie folgt:
- fileName: Der Name der extrahierten Datei
- createDate: Das Speicherdatum der Datei bei der Erzeugung durch ITScope
- fileSize: Die Dateigröße in Byte
- hash: Ein hash-Wert gebildet aus Dateiname, CreateDate und FileSize
- link: Der Link zum herunterladen der Datei

2. Bei jedem Download einer Datei sollte aus der o.g. Liste immer der Hash-Wert mit gespeichert werden. Damit kann die Prüfung
auf neue Inhalte bspw. stündlich erfolgen. Wenn die Ressource ``/info/scheduler/latest`` aufgerufen werden einfach die gespeicherten Hash-Werte
mit den Hash-Werten in ``/info/scheduler/latest`` verglichen.

Sind sie gleich, hat sich die Ressource nicht geändert bzw. stehen keine neuen Dateien zur Verfügung. Bei ungleichen Hash-Werten beginnt das Spiel von vorne.

3. Haben sich die Hash-Werte geändert, kann die url unter link verwendet werden um die jeweils aktuelle Datei herunterzuladen.
Die Dateien werden intern als Stream bereitgestellt, es ist hier egal wie langsam oder schnell die aufrufende Ressource bzw. die Bandbreite dazwischen ist,
der Heap-Verbrauch der Anwendung sollte max. ein paar Megabyte beanspruchen.


## Start und Verwendung über die lokalen Verzeichnisse
Es empfielt sich den Container mit docker-compose zu starten (wenn Docker als Container-Plattform zum Einsatz kommt).
Hierdurch lassen sich bequem die entsprechenden (lokalen) Ordner-Pfade bzw. Zielverzeichnisse festlegen.
Im folgenden wird eine docker-compose.yaml beschrieben, mit der sich der Container starten und konfigurieren lässt.
````yaml
version: "3.7"

services:
  streamzipper:
    container_name: streamzipper
    image: nexus.gretzki.ddns.net:10501/streamzipper:latest
    environment:
      - "ASPNETCORE_ENVIRONMENT=Development"
    volumes:
      - /path/on/your/local/system/streamzipper/backup/snapshots:/app/backup/snapshots
      - /path/on/your/local/system/streamzipper/backup/feeds:/app/backup/feeds
      - /path/on/your/local/system/streamzipper/data/snapshots:/app/data/snapshots
      - /path/on/your/local/system/streamzipper/data/feeds:/app/data/feeds
      - /path/on/your/local/system/streamzipper/config/configuration.json:/app/configuration.json
    expose:
      - "4000"
    ports:
      - "4000:4000"
    logging:
      driver: "json-file"
      options:
        max-size: "10k"
        max-file: "20"
    restart: always
...

````
Entscheidend ist die Konfiguration des volumes. Hierrüber werden die heruntergeladenen und entpackten Dateien in einem Verzeichnis auf dem
Docker-Host abgelegt und damit für die Weiterverarbeitung zugänglich gemacht.
Die Angaben der Ziele muss mit der Angabe der Verzsichnisse in der configuration.json übereinstimmen.
Bspw.
/path/on/your/local/system/streamzipper/data/snapshots:/app/data/snapshots --> "storeLocation" : "/app/data/snapshots/"

Usw.
Beim ersten Start müssen die, in der docker-compose.yaml angegebenen lokalen Volume-Pfade vorhanden sein.
Ansonsten schlägt der Container-Start fehl.

Die Ziele haben folgende Bedeutung:
- /app/data/snapshots --> hier befinden sich, in jeweiligen Unterordnern, die heruntergeladenen ZIP-Dateien des Snapshots, für eine eventuelle spätere Wiederwerwendung
- /app/data/feeds --> hier befinden sich, in jeweiligen Unterordnern, die heruntergeladenen ZIP-Dateien des Datenfeeds, für eine eventuelle spätere Wiederwerwendung
- /app/data/snapshots/latest --> hier befinden sich die aktuellen, zuletzt heruntergeladenen und entpackten Dateien des Snapshots (tägliche Inkremente) 
- /app/data/feeds/latest --> hier befinden sich die aktuellen, zuletzt heruntergeladenen und entpackten Dateien des Feeds (innertägliche Inkremente)

## Status
Aktuell stehen unterschiedliche URLs zur Verfügung, mit der sich verschiedene Zustände der Anwendung prüfen lassen.
Zusammengefasst werden diese Urls unter:
http(s)://IPorDOMAIN:Port/swagger/index.html

<img src="./StreamZipper/images/swagger.png" />




